import React, { useEffect, useRef } from 'react';
import '../styles/modal.css';

export default function Modal({ isOpen, close, children }) {

    const mountRef = useRef(null);

    useEffect(() => {
        const currentRef = mountRef.current;
        return () => {
            for (let i = currentRef.children.length - 1; i <= 0; i++) {
                const child = currentRef.children[i];
                currentRef.remove(child);
            }
        }
    }, []);

    const handleClick = (event) => {
        event.stopPropagation();
    }

    return (
        <article className={isOpen ? 'modal is-open' : 'modal'} >
            <button
                className="modal-close"
                onClick={() => { close() }}
            >X</button>
           
            <div className="container">
            <div className='cuadro'>
            <h2>Agregar producto</h2>
                
            
            <form>
      <label htmlFor="nombre">Nombre del producto:</label>
      <input type="text" id="nombre" name="nombre"></input>

      <label htmlFor="precio">Precio:</label>
      <input type="text" id="precio" name="precio"></input>

      <label htmlFor="descripcion">Descripción:</label>
      <textarea id="descripcion" name="descripcion"></textarea>

      <label htmlFor="imagen">Imagen:</label>
      <input type="file" id="imagen" name="imagen"></input>


      <div className="botones">
        <button type="submit">Guardar</button>
        <button type="button">Eliminar</button>
        
      </div>
      </form>
      </div>
    </div>
           
        </article>
    );
}