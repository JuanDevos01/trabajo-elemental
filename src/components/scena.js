import React, { useEffect, useRef, useState } from 'react';
import { Pannellum } from 'pannellum-react';
import Modal from '../components/modal';
import dataScene from '../helpers/dataScene';
import { UseModal } from '../hooks/useModal';
import ModelContainer from './modelContainer';
import DataScenes from '../helpers/dataScene2';

export default function Scene() {
    const { isOpen, openModal, closeModal } = UseModal(false);
    const [scene, setScene] = useState(dataScene['insideOne']);
    const [model, setModel] = useState(null);
    const [selectedImage, setSelectedImage] = useState(null);
    const [hotspots, setHotspots] = useState([]);
    const pannellumViewerRef = useRef(null);



    const mountRef = useRef(null);

    useEffect(() => {
        const currentRef = mountRef.current;
        return () => {
            for (let i = currentRef.children.length - 1; i >= 0; i--) {
                const child = currentRef.children[i];
                currentRef.removeChild(child);
            }
        }
    }, []);

    const agregarHotSpot = (pitch, yaw) => {
        const newHotspot = {
            type: 'custom',
            pitch,
            yaw,
            text: 'Nuevo hotspot',
            URL: 'https://ejemplo.com',
            
        };

        setHotspots([...hotspots, newHotspot]);
    };

    return (
        <div ref={mountRef} >
            <input type="file" accept="image/*" onChange={(e) => setSelectedImage(e.target.files[0])} />
            
                <Pannellum
                ref={pannellumViewerRef} 
                width={'89%'} 
                height={'80vh'} 
                title={scene.title} 
                image={selectedImage ? URL.createObjectURL(selectedImage) : scene.image} 
                pitch={-16.28} 
                yaw={-1.66} 
                hfov={130} 
                autoLoad
                showControls={false}
                showFullscreenCtrl={false} 
                showZoomCtrl={false} 
                orientationOnByDefault={true} 
                hotspotDebug={true} 
            >
                {hotspots.map((hotspot, index) => (
                    <Pannellum.Hotspot key={index} handleClick={() => { openModal() }} {...hotspot}  />
                ))}
            </Pannellum>
            
            
            <button style={{position: 'absolute', left: '10px', top: '68px'}} onClick={()=>agregarHotSpot(pannellumViewerRef.current.panorama.getPitch(), pannellumViewerRef.current.panorama.getYaw())}>Agregar Producto</button>
            <button onClick={()=> console.log(pannellumViewerRef.current.panorama.getPitch())}>get pitch</button>
            <button onClick={()=> console.log(hotspots[0])}>Prueba</button>
            <Modal isOpen={isOpen} close={() => closeModal()}>
                {isOpen && <ModelContainer nameModel={model} />}
            </Modal>
        </div>
    );
}
